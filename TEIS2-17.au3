;~ #include <SendAlert.au3>
;~ Configuracion del autrecord
Opt('WinWaitDelay', 1500)
Opt('WinDetectHiddenText', 1)
Opt('MouseCoordMode', 0)
; CargaDatos() ====================================================================================================================
; Author.........: William Aguirre - willianaguirre123@gmail.com
; Name...........: CargaDatos
; Description....: Esta funcion permite llenar los campos en la interfaz de recibos en el Waldbot desde el campo de cliente hasta el boton continuar.
; Syntax.........: CargaDatos($NComp,$FF,$MM,$TC)
; Parameters.....: $NComp		- Numero de comprobante
;				   $FF			- Fecha de emision de factura
;				   $MM			- Tipo de moneda
;				   $TC			- Tipo de cambio
; Return values..: Success - No retorna valor
;Example.........: CargaDatos($MatPrue [$x][3],$MatPrue [$x][2],$MatPrue [$x][4],$MatPrue [$x][5])
;
; ====
Func CargaDatos($NComp, $FF, $MM, $TC, $ind ,$ruc)
	$TC = StringReplace($TC, ".", "");El waltbot no recibe punto
	Sleep(500)
	_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gesti�n Integral Versi�n 11.0.23", "")
	MouseClick("left", 253, 159, 1)
	Sleep(500)
	$FlagUsd = SearchMovSuc($NComp, $MM)
	Sleep(500)
	Send("{SHIFTDOWN}{END}{SHIFTUP}{BACKSPACE}")
	Send(SearchConcepto($NComp))
	Sleep(500)
	Send("{TAB}{TAB}{TAB}")
	Sleep(500)
	Send("{END}{BACKSPACE}{BACKSPACE}{BACKSPACE}{BACKSPACE}{BACKSPACE}{BACKSPACE}{BACKSPACE}{BACKSPACE}{BACKSPACE}")
	Sleep(2000)
	Send($FF)
	Sleep(500)
	Send("{TAB}")
	Sleep(500)
	If $FlagUsd = 1 Then
		Send("{SHIFTDOWN}{END}{SHIFTUP}{BACKSPACE}")
		Send($TC)
	EndIf
	Sleep(500)
	Send("{TAB}{TAB}{ENTER}")
	Sleep(1500)
	If WinActivate("Recibos", "La fecha de emisi�n no se encuentra dentro del rango de fechas establecidas en par�metros") Then
		_WinWaitActivate("Recibos", "La fecha de emisi�n no se encuentra dentro del rango de fechas establecidas en par�metros")
		Send("{ENTER}")
		$alert = "error en fecha de emisi�n"
		Send_Email($ruc,$NComp, $alert)
		ConsoleWrite("Envia correo" & $alert & @CRLF)
		Send("{ENTER}")
		Return 1
	Else
		Return 0
	EndIf

EndFunc   ;==>CargaDatos
; SearchMovSuc() ====================================================================================================================
; Author.........: William Aguirre - willianaguirre123@gmail.com
; Name...........: SearchMovSuc
; Description....: Esta funcion verifica el tipo de movimiento y sucursal dentro de la carga de datos.
; Syntax.........: SearchMovSuc($p1,$p2)
; Parameters.....: $p1		- Numero de comprobante
;				   $p2			- Fecha de emision de factura
; Return values..: Success - No retorna valor
;Example.........: SearchMovSuc($NComp,$MM)
;
; ====
Func SearchMovSuc($p1, $p2)
	$Opti = StringInStr($p1, "001-002")
	$Tei = StringInStr($p1, "001-001")
	Global $USD = StringInStr($p2, "D�LAR AMERICANO")
	$Gua = StringInStr($p2, "Guarani")
	If $Opti = 1 And $USD = 1 Then
		Send("{HOME}{TAB}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{ENTER}")
		Sleep(1000)
		Send("{HOME}{DOWN}{DOWN}{ENTER}")
	ElseIf $Opti = 1 And $Gua = 1 Then
		Send("{HOME}{TAB}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{ENTER}")
		Sleep(1000)
		Send("{HOME}{DOWN}{DOWN}{ENTER}")
	ElseIf $Tei = 1 And $USD = 1 Then
		Send("{HOME}{TAB}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{ENTER}")
		Sleep(1000)
		Send("{ENTER}")
	ElseIf $Tei = 1 And $Gua = 1 Then
		Send("{HOME}{TAB}{DOWN}{DOWN}{DOWN}{DOWN}{ENTER}")
		Sleep(1000)
		Send("{ENTER}")
	EndIf
	ConsoleWrite("Sucursal" & $Opti & $Tei & $USD & $Gua & @CRLF)
	Return $USD
EndFunc   ;==>SearchMovSuc
; SearchConcepto() ====================================================================================================================
; Author.........: William Aguirre - willianaguirre123@gmail.com
; Name...........: SearchConcepto
; Description....: Esta funcion inserta el la informacion en el campo concepto.
; Syntax.........: SearchConcepto($c1)
; Parameters.....: $c1		- Numero de comprobante
; Return values..: Success - No retorna valor
;Example.........: SearchConcepto($c1)
;
; ====
Func SearchConcepto($c1)
	$Opti = StringInStr($c1, "001-002")
	$Tei = StringInStr($c1, "001-001")
	If $Opti = 1 Then
		$NumFac = StringReplace($c1, "001-002-", "")
	ElseIf $Tei = 1 Then
		$NumFac = StringReplace($c1, "001-001-", "")
	EndIf
	$SendCon = "Ret_Fat_" & $NumFac
	Return $SendCon
EndFunc   ;==>SearchConcepto
