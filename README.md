# Carga Retenciones Clientes

Proceso de automatizacion de carga de retenciones provenientes de clientes en el sistema wallbott
1.Para Iniciar el proceso es necesario que el outlook y el wallbott esten cerrados 
2.Se deben modificar las variables del archivo GlobalConstant.au3 [correo donde de enviasn alertas,
 carpeta donde se guardan ficheros, carpeta del outlook donde están las facturas a procesar, clave del WallBot]
3. Para correr el proceso es el script llamado Master_historiador.au3 , al abrirlo y ejecutar F7 se genera 
   el .exe para ejecutar [Este paso es para crear el ejecutable].
4. Si no existen facturas a procesas (correos sin leer en el outlook) saldrá un mensaje de alerta, solo se debe
cerrar la ventana manualmente y esperar que existan facturas aa procesar para ejecutar el robot
5. las alertas se envian una a una por cada retención que genere problemas [RUC , fecha , Saldo ML ,
  Comprobante cobrado] .
6. Validar la sesión que se ingresa [el proceso se ejecuta en la que este por defecto para el caso de pruebas el DEMO].
   Esta ubicado en la primera fila , Se debe cambiar el orden en el wallbott.
7. En caso tal de que se interrumpa el proceso se debe cambiar el estado del correo como no leido manualmente y volver 
 a ejecutar el proceso . Se recomienda tener solo las retenciones a procesar dentro de la carpeta del outlook.
8. En la dirección RET_AÑO_MES_DIA se encuentra el archivo de texto facturasdias.txt donde se adjuntan las retenciones
 que ha realizado el robot numero de control y fecha . Estas carpetas se pueden eliminar son solo registro no afecta la 
 progrmamación del robot .
 