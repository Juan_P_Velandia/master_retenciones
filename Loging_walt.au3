;~ #include <GlobalConstant.au3>

;~ Configuracion de parametros espera , mouse ,
Opt('WinWaitDelay', 1500)
Opt('WinDetectHiddenText', 1)
Opt('MouseCoordMode', 0)
; Ini_sesion() ====================================================================================================================
; Author.........: Juan Pablo Velandia Suárez -- jvelandia33@outlook.com
; Name...........: Ini_SEsion
; Description....: Ejecuta la aplicación de waldbot , ingresa con la contraseña rpa2019
; Syntax.........: Ini_sesion()
; Return values..: Success  1
;Example.........: Ini_sesion
;
; ====

Func Ini_sesion($pass)
	ShellExecute("C:\Waldbott Software\Gestión Integral 11\Sgiw11.exe")
	_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gestión Integral Versión 11.0.23", "")
	Send("{ALTDOWN}{ALTUP}{ENTER}{ENTER}")
	_WinWaitActivate("Seleccionar Empresa", "")
	Send("{ENTER}")
;~ 	Send("{DOWN 2}{ENTER}")
	_WinWaitActivate("Inicio de Sesión", "")
;~ 	Local $pass = $password
	Send($pass)
	Send("{ENTER}")
	_WinWaitActivate("Gestión de Cobranzas", "")
	Send("{ENTER}")
	Return 1
EndFunc   ;==>Ini_sesion
Func Exit_recibo()
	_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gestión Integral Versión 11.0.23", "")
	MouseClick("left", 750, 476, 1)
	Send("{ESC}{ESC}")
EndFunc   ;==>Exit_recibo
