
Global $mat
#include <MsgBoxConstants.au3>
;~ Configuracion del autrecord
Opt('WinWaitDelay', 1500)
Opt('WinDetectHiddenText', 1)
Opt('MouseCoordMode', 0)
; Saldo_Cero() ====================================================================================================================
; Author.........: Juan Pablo Velandia Suarez - jvelandia33@outlook.com
; Name...........:  Validación Saldo ML
; Description....: Esta funcion valida el saldo ML entre recibos cobrados y retenciones generadas
; Syntax.........: Saldo_Cero($F_emision, $orden,$certificado,$comprobante_ret,$moneda,$taza_cambio)
; Parameters.....:
;~ 				   $F_emision ;Fecha de emisión de la retención
;~ 				   $$Orden ; numero interno
;~ 				   $certificado ; numero interno
;~ 				   $imp_retenido;Valor de la retención
;~ 				   $comprobante_ret;Campo comprobante retenido que no se utiliza
;~ 				   $modena ; tipo de moneda Guarani o dolares
;~ 				   $taza_cambio ;taza de cambio de guarani dolares
; Return values..: Success - 1
;Example.........: Saldo_Cero($F_emision, $orden,$certificado,$comprobante_ret,$moneda,$taza_cambio)
;
; ====
Func Saldo_Cero($F_emision, $orden, $certificado, $comprobante_ret, $moneda, $taza_cambio, $num_control, $ind, $iva, $renta)
;~ 	adquirir comprobante cobrado
	Local $diferencia = 0
	$iva = StringReplace($iva, ".", "") ;El waltbot no recibe punto
	$renta = StringReplace($renta, ".", "") ;El waltbot no recibe punto
	$taza_cambio = StringReplace($taza_cambio, ".", "") ;El waltbot no recibe punto
	ConsoleWrite("RENTA ES " & $renta & @CRLF)
	ConsoleWrite("IVA ES " & $iva & @CRLF)
	Local $importe_completo = $iva + $renta
	Local $importe_2 = $importe_completo
	ConsoleWrite("IMPORTE " & $importe_completo & @CRLF)
	Local $importe_1 = 0
	Sleep(500)
	_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gestión Integral Versión 11.0.23", "")
	MouseClick("left", 517, 346, 1)
	MouseClick("left", 499, 394, 1)
	Send("{ENTER}")
	Sleep(500)
	_WinWaitActivate("Comprobante a Cobrar", "Importe &Sin Impuest")
	Send("{CTRLDOWN}c{CTRLUP}") ;{TAB 3}{ENTER}")

	If $moneda = "GUARANI" Then
		$importe_1 = ClipGet()
		$importe_1 = StringReplace($importe_1, ",", ".") ;El waltbot no recibe punto
		$importe_1 = StringReplace($importe_1, ".", "") ;El waltbot no recibe punto
		Sleep(500)
		If $importe_1 >= ($importe_completo) Then
			correcion_comp($importe_completo)
			$importe_1 = $importe_completo
		EndIf
	Else
		$importe_1 = ClipGet()
		$importe_1 = StringReplace($importe_1, ",", ".") ;El waltbot no recibe punto
		$importe_1 = StringReplace($importe_1, ".", "") ;El waltbot no recibe punto
		ConsoleWrite("Importe copiado" & $importe_1 & @CRLF)
		$importe_1 = $importe_1 * $taza_cambio
		ConsoleWrite("IMPORTE Copiado* Dolares " & $importe_1 & @CRLF)
		If $importe_1 >= ($importe_completo) Then
			Local $aux1 = $importe_completo / $taza_cambio
			$aux1 = Round($aux1, 2)
			correcion_comp($aux1)
			ConsoleWrite("IMPORTE_DOLARES" & $aux1)
			$importe_1 = $aux1 * $taza_cambio
			ConsoleWrite("IMPORTE " & $importe_1 & @CRLF)
		EndIf
	EndIf
	Sleep(500)
	Send("{TAB}")
	MouseClick("left", 577, 54, 1)
;~ 	adquirir valor de la retencion IVA y Renta
	Sleep(750)
	If WinActivate("Comprobante a Cobrar", "El Saldo del comprob") Then
		Sleep(500)
		MouseClick("left", 289, 145, 1)
		_WinWaitActivate("Comprobante a Cobrar", "Importe &Sin Impuest")
		MouseClick("left", 562, 84, 1)
	EndIf
	$diferencia = $importe_1 - $importe_2
;~ 	$diferencia = StringFormat("%.0f", $diferencia)  ; Ajuste a solo dos decimales
	$diferencia = NumeroEntero($diferencia)
	ConsoleWrite("DIFERENCIA" & $diferencia & @CRLF)
	If $diferencia = 0 Then
		ConsoleWrite("Saldo ML es igual a 0" & @CRLF)
		Enviar_Comprobante($num_control, $ind, $orden)
		Return 1
	ElseIf $diferencia > -5000 And $diferencia < 100 Then ;Ajuste del Saldo ML
		ConsoleWrite("Saldo ML es igual a 0 con ajuste " & @CRLF)
		Retencion_IVA("OTRO", $F_emision, $orden, $certificado, $diferencia, $comprobante_ret)
		Enviar_Comprobante($num_control, $ind, $orden)
		Return 1
	Else
		ConsoleWrite("Saldo ML es diferente de 0" & @CRLF)
		Local $alert = "error por saldo ML"
		Send_Email($ind, $orden, $alert)
		ConsoleWrite("Envia correo motivos SALDO ML" & @CRLF)
		Return 0
	EndIf
EndFunc   ;==>Saldo_Cero
Func Enviar_Comprobante($num_control, $ind, $orden)
	_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gestión Integral Versión 11.0.23", "")
	MouseClick("left", 834, 630, 1)
	Sleep(500)
	If WinActivate("Recibos", "El Saldo del recibo ") Then
		_WinWaitActivate("Recibos", "El Saldo del recibo ")
		MouseClick("left", 228, 129, 1)
		_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gestión Integral Versión 11.0.23", "")
		Sleep(500)
		Send("{ESC}{ESC}")
		_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gestión Integral Versión 11.0.23", "")
		Sleep(500)
		Send("{ESC}{ESC}")
		$alert = "SALDO ML DIFERENTE"
		;Correo alerta enviar
		Send_Email($ind, $orden, $alert)
		ConsoleWrite("Envia correo por saldo ML Diferente desde comprobante" & @CRLF)
	Else
		Sleep(500)
		_WinWaitActivate("Comentario", "")
		Sleep(500)
		MouseClick("left", 538, 304, 1)
		Sleep(750)
		_WinWaitActivate("Número Sugerido", "")
		Sleep(500)
		Send($num_control)
		Sleep(1500)
		Send("{TAB 3}{ENTER}")
;~ 		_WinWaitActivate("Recibos", "")
		ConsoleWrite("Cargar Codigo de control")
		Sleep(1500)
		If WinActivate("Recibos", "No puede utilizar esta numeración ya que existen movimientos de Clientes con la misma.") Then
;~ 			_WinWaitActivate("Recibos", "No puede utilizar esta numeración ya que existen movimientos de Clientes con la misma.")
			_WinWaitActivate("Recibos", "No puede utilizar es")
			Send("{ESC 2}")
			ConsoleWrite("SI ha ingresado")
			Sleep(500)
			Send("{ESC 2}")
			$alert = "Comprobante ya cargado"
			Send_Email($ind, $orden, $alert)
			ConsoleWrite("Comprobante ya cargado" & @CRLF)
		EndIf
		Sleep(500)
		Send("{ESC 2}")
		Sleep(500)
		Send("{ESC 2}")
		Return 1
	EndIf
EndFunc   ;==>Enviar_Comprobante
;~ ControlGetHandle
Func correcion_comp($TC)
	Send("{SHIFTDOWN}{END}{SHIFTUP}{BACKSPACE}")
	Send($TC)
EndFunc   ;==>correcion_comp
Func NumeroEntero($data)
	$data = StringFormat("%.2f", $data)
	ConsoleWrite($data&@CRLF)
	$aString = StringSplit($data, "")
	For $i = 1 To $aString[0]
		If $aString[$i] == "." Then
			Local $pos = $i
;~ 			ConsoleWrite($pos&@CRLF)
			If $aString[$i + 1] >= 5 And $aString[$i + 2] >= 0  Then
					$data = Round($data, 0) ;Redonde hacia arriba
			Else
				$data = StringLeft($data, $pos - 1)
			EndIf
			$data = StringLeft($data, $pos - 1) ;redondea hacia abajo
			ExitLoop
		EndIf
	Next
	Return $data
EndFunc   ;==>NumeroEntero
