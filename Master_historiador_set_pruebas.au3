#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Juan Pablo Velandia, William Aguirre

 Script Function:
	Master Integrador Secuencia del Programa Retenciones Clientes Waltbot

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
#include <Loging_walt.au3> ;Tarea 1 Iniciar
#include <Buscar_ruc.au3> ;Tarea 2 Buscar el RUC Cliente
#include <PruebaLectMatrizReturn.au3> ;LEER la Info del correo
#include <TEIS2-17.au3> ;Tarea 3 Llenar campos de la factura
#include <Retencion_iva.au3> ;Tarea 4 Crear Retenciones tipo IVA o RENTA
#include <Tarea_5.au3> ;Tarea 5 Ingresar el Comprobante cobrado
#include <Saldo_ML.au3> ;Tarea 6 Validar Saldo ML de la factura y 7 enviar comprobante
#include <GlobalConstant.au3>
Global $mat[10][17]
Local $Flag_ruc = 0
Local $Flag = 0
;~ Modificaciones integracion- Matriz de facturas validas

;Factura1 RUC NO encontrado
;~ $mat[0][0] = "80009641-0" ;RUC cambiado no se encontro
$mat[0][0] = "11111111-0" ;RUC cambiado no se encontro
$mat[0][2] = "23/07/2020" ;FECHA de Emision
$mat[0][3] = "001-002-0098420"
$mat[0][4] = "Guarani"
$mat[0][5] = ""
$mat[0][6] = "1484182" ;Total de Importe
$mat[0][10] = "742091" ;Total RETENIDO RENTA
$mat[0][11] = "742091" ;Total Retenido IVA 10%
$mat[0][12] = "7E13879D" ;Codigo de control

;Factura 6 Fecha no valida
$mat[1][0] = "80009641-0" ;RUC cambiado no se encontro
$mat[1][2] = "16/06/2020" ;FECHA de Emision
$mat[1][3] = "001-002-0099420"
$mat[1][4] = "Guarani"
$mat[1][5] = ""
$mat[1][6] = "1484182" ;Total de Importe
$mat[1][10] = "742091" ;Total RETENIDO RENTA
$mat[1][11] = "742091" ;Total Retenido IVA 10%
$mat[1][12] = "7E23079D" ;Codigo de control

;Factura 2 Retencion con ajuste retención otras
;~ $mat[1][0] = "80006251-5" ;RUC
;~ $mat[1][1] = "00100200099269" ;numero comprobante de retencion no se esta usando
;~ $mat[1][2] = "05/08/2020" ;Fecha de emisión
;~ $mat[1][3] = "00100200099269" ;tener en encuenta que son 3 ceros en walt
;~ $mat[1][4] = "Guarani" ;Tipo de Moneda
;~ $mat[1][5] = "" ;taza de cambio si lo hay
;~ $mat[1][6] = "2842623" ;Total General Retenido
;~ $mat[1][10] = "0" ;Total RETENIDO RENTA
;~ $mat[1][11] = "2842620" ;Total Retenido IVA 10%
;~ $mat[1][12] = "A1B2197D" ;Codigo de control

;Factura 3 Factura sin el comprobante DOLar
$mat[2][0] = "80026157-7"
$mat[2][2] = "06/08/2020"
$mat[2][3] = "001-001-0030047";Para no encontrarla
$mat[2][4] = "DÓLAR AMERICANO"
$mat[2][5] = "6935"
$mat[2][6] = "208050"
$mat[2][10] = "0" ;Total RETENIDO RENTA
$mat[2][11] = "208050" ;Total Retenido IVA 10%
$mat[2][12] = "A1A2097D" ;Codigo de control

;Factura 4;IVA RENTA OK
$mat[3][0] = "80009641-0" ;RUC cambiado no se encontro
;~ $mat[3][0] = "11111111-0" ;RUC cambiado no se encontro
$mat[3][2] = "23/07/2020" ;FECHA de Emision
$mat[3][3] = "001-002-0099420"
$mat[3][4] = "Guarani"
$mat[3][5] = "10"
$mat[3][6] = "1484182" ;Total de Importe
$mat[3][10] = "742091" ;Total RETENIDO RENTA
$mat[3][11] = "742091" ;Total Retenido IVA 10%
$mat[3][12] = "7E23079D" ;Codigo de control

;Factura 5 Retencion saldo ML no esta en el rango
$mat[4][0] = "80006251-5" ;RUC
$mat[4][1] = "00100200099269" ;numero comprobante de retencion no se esta usando
$mat[4][2] = "05/08/2020" ;Fecha de emisión
$mat[4][3] = "001-002-00099269" ;tener en encuenta que son 3 ceros en walt
$mat[4][4] = "Guarani" ;Tipo de Moneda
$mat[4][5] = "" ;taza de cambio si lo hay
$mat[4][6] = "2842623" ;Total General Retenido
$mat[4][10] = "0" ;Total RETENIDO RENTA
$mat[4][11] = "2842621" ;Total Retenido IVA 10%
$mat[4][12] = "A1A2097D" ;Codigo de control

;Factura 6 Fecha no valida
;~ $mat[5][0] = "80009641-0" ;RUC cambiado no se encontro
;~ $mat[5][2] = "16/06/2020" ;FECHA de Emision
;~ $mat[5][3] = "001-002-0099420"
;~ $mat[5][4] = "Guarani"
;~ $mat[5][5] = ""
;~ $mat[5][6] = "1484182" ;Total de Importe
;~ $mat[5][10] = "742091" ;Total RETENIDO RENTA
;~ $mat[5][11] = "742091" ;Total Retenido IVA 10%
;~ $mat[5][12] = "7E23079D" ;Codigo de control

;~ -----------------------------------------------------------------------------------------------------------------------------

;~ $mat = LecturaMailRet()
$Elementos = 4
;~ show_matriz(3, 17, $mat)
_ArrayDisplay($mat)
Ini_sesion($password) ;Tarea 1
For $x = 0 To $Elementos Step 1
	$Flag_ruc = Buscar_RUC($mat[$x][0],$mat[$x][3]) ;Tarea 2 (ruc ,num interno)
	If $Flag_ruc = 1 Then
		$Flag = CargaDatos($mat[$x][3], $mat[$x][2], $mat[$x][4], $mat[$x][5], $x ,$mat[$x][0]) ;Tarea 3
		If $Flag = 0 Then
			ConsoleWrite("cargando retencion")
			Retencion_IVA("IVA", $mat[$x][2], $mat[$x][3], $mat[$x][3], $mat[$x][11], "0") ;Tarea 4 IVA
			Retencion_IVA("RENTA", $mat[$x][2], $mat[$x][3], $mat[$x][3], $mat[$x][10], "0") ;Tarea 4 RENTA
			com_cobrado($mat[$x][3], $mat[$x][4], $mat[$x][5], $mat[$x][10], $mat[$x][11]) ;Tarea 5 $num_interno, $moneda, $tasa_cambio,$iva,$renta)
;~ 			Saldo_Cero($mat[$x][2], $mat[$x][3], $mat[$x][3], "0", $mat[$x][4], $mat[$x][5], $mat[$x][12],  $mat[$x][0]) ;Tarea 6 ,7
		EndIf
	EndIf

	Sleep(5000)
	MouseClick("left", 751, 253, 1)
	MouseClick("left", 750, 253, 1)
	Send("{ESC}{ESC}")
	Send("{ESC 4}")     ;SALIR DE LA FACTURA
	Sleep(1000)
	Send("{ESC 2}")
	Sleep(1000)
	Send("{ESC 2}")
Next
Send("{ALTDOWN}{F4}{ALTUP}");Close waltbot
_ArrayDisplay($mat)
;Configuración Correo
Func Send_Email($RUC, $num_int, $error)
	ShellExecute("C:\Users\rpa\Desktop\Microsoft Office Outlook 2007.lnk");Ejecutar Outlook
	$oOL = _OL_Open();Abrir Outlook
	Local $dir = "jvelandia33@unisalle.edu.co";Dirección Asociada
	Local $affair = "Error en Clientes Retención";Asunto
	Local $body = "<b>Retención RUC :" & $RUC& @CRLF & "numero interno:" & $num_int&@CRLF & "Rechazado por:" & $error
	_OL_Wrapper_SendMail($oOL, $dir, "", "", $affair, $body, "", $olFormatHTML, $olImportanceHigh)
	_OL_Close($oOL);Cerrar Outlook
	Sleep(5000)
	Send("{ENTER}") ;Saltar el anuncio de comprobantes sin enviar
EndFunc   ;==>Send_Email
;~ Funciones Globales
Func _WinWaitActivate($title, $text, $timeout = 0)
	WinWait($title, $text, $timeout)
	If Not WinActive($title, $text) Then WinActivate($title, $text)
	WinWaitActive($title, $text, $timeout)
EndFunc   ;==>_WinWaitActivate

Func show_matriz($N, $items, $mat)
	For $i = 0 To $N - 1
		For $j = 0 To $items - 1
			ConsoleWrite($mat[$i][$j] & " ")
		Next
		ConsoleWrite(@CRLF)
	Next
	ConsoleWrite(@CRLF)
	ConsoleWrite(@CRLF)
EndFunc   ;==>show_matriz
