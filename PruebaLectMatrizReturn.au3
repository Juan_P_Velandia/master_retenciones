#include <OutlookEX.au3>
#include <String.au3>
;~ #include <GlobalConstant.au3>
Global $aAttachments

; LecturaMailRet() ====================================================================================================================
; Author.........: William Aguirre - willianaguirre123@gmail.com
; Name...........: LecturaMailRet
; Description....: Esta funcion permite extraer los datos necesarios del correo para la carga de retenciones en el Wallbot, identifica
;				   el tipo de moneda y el tipo de cambio de ser necesario.
; Syntax.........: LecturaMailRet()
; Parameters.....:
; Return values..: Success - Retorna un array de dos dimensiones "[a][b]" donde a corresponde al numeracion de almacenamiento de
;				   factura y b apunta a los siguientes datos:
;                  |0  - RUC
;                  |1  - Comprobante de Retenci�n Virtual
;                  |2  - Fecha Emisi�n
;                  |3  - N�mero de Comprobante
;                  |4  - Moneda
;                  |5  - TIPO DE CAMBIO (si lo hay)
;				   |6  - TOTAL GENERAL RETENIDO
;				   |7  - Numero interno de cliente
;				   |8  - Descirpcion
;				   |9  -Total Importado
;				   |10 -Total Retenido Renta
;				   |11 -Total Retenido IVA 10%
;				   |12 -Codigo de control
;Example.........: $array=LecturaMailRet()           ;array que retorna la funcion
;				   $Elementos                        ;variable global que contiene el numero de facturas a procesar
;
; ====

Func LecturaMailRet($DirIn,$DirRet)
	; Acceso a la aplicaci�n
	Global $oOutlook = _OL_Open()
	If @error <> 0 Then Exit MsgBox(16, "OutlookEX UDF", "Error opening connection to Outlook. @error = " & @error & ", @extended = " & @extended)

	; Acceso al Inbox
	Global $aFolder = _OL_FolderAccess($oOutlook, $DirIn, $olFolderInbox); especificaion de carpeta de la cual se extraen los correos
	If @error <> 0 Then Exit MsgBox(16, "OutlookEX UDF", "Error accessing the Inbox. @error = " & @error & ", @extended = " & @extended)

	; Busqueda de correos sin leer
	Global $sFilter = "@SQL=""urn:schemas:httpmail:read""=0"
	Global $aResult = _OL_ItemSearch($oOutlook, $aFolder[1], $sFilter, "EntryID,subject,SenderEmailAddress")
	If @error Then
		ConsoleWrite("no existen correos"&@CRLF)
		MsgBox(48,"Alerta","Sin facturas en el correo")
		Exit 0
	EndIf

	; Obtiene el correo en archivos de texto plano
	For $i = 1 to (UBound($aResult)-1)
		$oItem = $oOutlook.Session.GetItemFromID($aResult[$i][0], Default) ; con el Id de Array $aResult , se obtiene el Item
		$oItem.GetInspector
		$sBody = $oItem.Body ; cuerpo del mensaje

		$oItem.Unread = False 				; cambia el estado de los mails a leido!!!!

		DirCreate($DirRet) ;crea caroeta de retenciones diaria
		$Dir = $DirRet  ;definicion ruta almacenamiento de archivo
		$archivo = FileOpen($Dir & ("BodyMail" & $i & ".txt"), 2)  ;creacion de archivo
		FileWrite($archivo, $sBody  & @CRLF)  ; escritura del cuerpo
		FileClose($archivo)  ;cerrar archivo
	Next
	_OL_Close($oOutlook)


	Global $Arc [UBound($aResult)-1][17]
	Global $Elementos = UBound($aResult)-1

	;Busqueda de informaci�n dentro del archivo de texto y generaci�n de archivo con datos filtrados
	For $j = 1 to (UBound($aResult)-1)
		$archivo = FileOpen($Dir & ("BodyMail" & $j & ".txt"), 0)     ; Abrir archivo
		$aux = 0   ;banderia lectura del primer RUC
		$FlagUsd = 0   ;Bandera de tipo de moneda

		For $k = 1 To 500
			$linea = FileReadLine($archivo, $k)
			Local $BRuc = StringInStr($linea, "RUC ")
			Local $BComRetV = StringInStr($linea, "Comprobante de Retenci�n Virtual")
			Local $BFEcha = StringInStr($linea, "Fecha Emisi�n:")
			Local $BNumComp = StringInStr($linea, "N�mero de Comprobante:")
			Local $BMon = StringInStr($linea, "Moneda:")
			Local $BTCam = StringInStr($linea, "TIPO DE CAMBIO:")
			Local $BTGRet = StringInStr($linea, "TOTAL GENERAL RETENIDO")
			Local $BTRet = StringInStr($linea, "TOTAL RETENIDO")
			Local $BImRet = StringInStr($linea, "Importe Retenido")
			Local $BCodC = StringInStr($linea, "C�digo Control")
			If $aux=0 And $BRuc>0 Then
				$Ruc = $linea
				$aux = 1
			ElseIf $BComRetV >0 Then
				While 1
					$k = $k + 1
					$linea = FileReadLine($archivo, $k)
					If $linea <> "" then
						$ComRetV = "Comprobante de Retenci�n Virtual: " & $linea
						ExitLoop
					EndIf
				WEnd
			ElseIf $BFEcha >0 Then
				$Fecha = $linea
			ElseIf $BNumComp >0 Then
				$NumComp = $linea
			ElseIf $BMon >0 Then
				$Mon = $linea
				Local $BTCam = StringInStr($linea, "TIPO DE CAMBIO:")
				If $BTCam >0 Then
					$TCam = $linea
					$FlagUsd = 1
				EndIf
			ElseIf $BTGRet  >0 Then
				$TGRet = $linea
			ElseIf $BTRet >0 Then
				$TRet = $linea
				$TRet = StringReplace($TRet, "TOTAL RETENIDO", "")
				$ParRet = _StringBetween($TRet,@TAB,@TAB)
				$Arc[$j-1][10] = $ParRet[0]
				$Arc[$j-1][11] = $ParRet[4]
			ElseIf $BImRet >0 Then
				$ImRet = $linea
				$ImRet = StringReplace($ImRet, "Importe Retenido", "")
				$ParRet = _StringBetween($ImRet,@TAB,@TAB)
				$Arc[$j-1][10] = $ParRet[0]
				$Arc[$j-1][11] = $ParRet[1]
			ElseIf $BCodC >0 Then
				$CodC = $linea
			EndIf
			if @error = -1 Then ExitLoop
		Next
		$aux = 0
		;Almacenado de los datos dentro del array
		If $FlagUsd = 1 Then  ;en el caso de que sea en dolares
			$Ruc = StringReplace($Ruc, "RUC", "")
			$Ruc = StringReplace($Ruc, " ", "");RUC espacios en el RUC
			$ComRetV = StringReplace($ComRetV, "Comprobante de Retenci�n Virtual:", "")
			$Fecha = StringReplace($Fecha, "Fecha Emisi�n:", "")
			$NumComp = StringReplace($NumComp, "N�mero de Comprobante:", "")
			$Mon = "D�LAR AMERICANO"
			$TCam = StringReplace($TCam, "Moneda:"&$Mon&"TIPO DE CAMBIO:", "")
			$TGRet = StringReplace($TGRet, "TOTAL GENERAL RETENIDO", "")
			$CodC = StringReplace($CodC, "C�digo Control ", "")
			$Arc[$j-1][0] = $Ruc
			$Arc[$j-1][1] = $ComRetV
			$Arc[$j-1][2] = $Fecha
			$Arc[$j-1][3] = $NumComp
			$Arc[$j-1][4] = $Mon
			$Arc[$j-1][5] = $TCam
			$Arc[$j-1][6] = $TGRet
			$Arc[$j-1][12] = $CodC
			$FlagUsd = 0
		Else   ;en el caso en que sea en guaranies
			$Ruc = StringReplace($Ruc, "RUC", "")
			$Ruc = StringReplace($Ruc, " ", "");RUC espacios en el RUC
			$ComRetV = StringReplace($ComRetV, "Comprobante de Retenci�n Virtual:", "")
			$Fecha = StringReplace($Fecha, "Fecha Emisi�n:", "")
			$NumComp = StringReplace($NumComp, "N�mero de Comprobante:", "")
			$Mon = StringReplace($Mon, "Moneda:", "")
			$TGRet = StringReplace($TGRet, "TOTAL GENERAL RETENIDO", "")
			$CodC = StringReplace($CodC, "C�digo Control ", "")
			$Arc[$j-1][0] = $Ruc
			$Arc[$j-1][1] = $ComRetV
			$Arc[$j-1][2] = $Fecha
			$Arc[$j-1][3] = $NumComp
			$Arc[$j-1][4] = $Mon
			$Arc[$j-1][5] = ""
			$Arc[$j-1][6] = $TGRet
			$Arc[$j-1][12] = $CodC
		EndIf
	Next

	Return $Arc ;retorna el arreglo con la informacion de los correos
EndFunc