#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Juan Pablo Velandia, William Aguirre

 Script Function:
	Master Integrador Secuencia del Programa Retenciones Clientes Waltbot

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
#include <Loging_walt.au3> ;Tarea 1 Iniciar
#include <Buscar_ruc.au3> ;Tarea 2 Buscar el RUC Cliente
#include <PruebaLectMatrizReturn.au3> ;LEER la Info del correo
#include <TEIS2-17.au3> ;Tarea 3 Llenar campos de la factura
#include <Retencion_iva.au3> ;Tarea 4 Crear Retenciones tipo IVA o RENTA
#include <Tarea_5.au3> ;Tarea 5 Ingresar el Comprobante cobrado
#include <Saldo_ML.au3> ;Tarea 6 Validar Saldo ML de la factura y 7 enviar comprobante
#include <GlobalConstant.au3>
;~ Global $mat[10][17]
;~ Global $mat[1][17]
Local $Flag_ruc = 0
Local $Flag = 0
Local $contador_t = 0
;~ -----------------------------------------------------------------------------------------------------------------------------

$mat = LecturaMailRet($DirInbox, $DirRetArc)

;~ _ArrayDisplay($mat)  ;muestra los datos a procesar

Ini_sesion($password) ;Tarea 1
For $x = 0 To $Elementos - 1 Step 1
	$Flag_ruc = Buscar_RUC($mat[$x][0], $mat[$x][3]) ;Tarea 2 (ruc ,num interno)
	If $Flag_ruc = 1 Then
		$Flag = CargaDatos($mat[$x][3], $mat[$x][2], $mat[$x][4], $mat[$x][5], $x, $mat[$x][0]) ;Tarea 3
		If $Flag = 0 Then
			ConsoleWrite("cargando retencion")
			Retencion_IVA("IVA", $mat[$x][2], $mat[$x][1], $mat[$x][1], $mat[$x][11], "0") ;Tarea 4 IVA
			Retencion_IVA("RENTA", $mat[$x][2], $mat[$x][1], $mat[$x][1], $mat[$x][10], "0") ;Tarea 4 RENTA
			Sleep(500)
			com_cobrado($mat[$x][3], $mat[$x][4], $mat[$x][5], $mat[$x][10], $mat[$x][11]) ;Tarea 5 $num_interno, $moneda, $tasa_cambio,$iva,$renta)
			Sleep(500)
			Saldo_Cero($mat[$x][2], $mat[$x][1], $mat[$x][1], "0", $mat[$x][4], $mat[$x][5], $mat[$x][12], $mat[$x][0], $mat[$x][10], $mat[$x][11])    ;Tarea 6 ,7
		EndIf

	EndIf
	generar_control_factura()
	Cerrar_factura()

Next
cerrar_ventanas()
_ArrayDisplay($mat)  ;muestra los datos a procesados
;~ Funciones
Func generar_control_factura()
	Sleep(5000)
	MouseClick("left",831,649,1)
	MouseClick("left",805,655,1)
	Sleep(200)
	MouseClick("left", 751, 253, 1)
	MouseClick("left", 750, 253, 1)
	Send("{ESC}{ESC}")
	Send("{ESC 4}")
	Sleep(200)
	Send("{ESC 2}")
	Sleep(200)
	Send("{ESC 2}")
	$contador_t = $contador_t + 1
	ConsoleWrite(@CRLF & $contador_t & "Factura" & $mat[$x][3] & $mat[$x][2] & @CRLF)
	Local $mesage = @CRLF & $contador_t & "Factura" & $mat[$x][3] & "  " & $mat[$x][2] & @CRLF
	Local $archivo_f = FileOpen($DirRetArc & ("facturasdias.txt"), 1)      ;creacion de archivo
	FileWrite($archivo_f, $mesage & @CRLF)   ; escritura del cuerpo
	FileClose($archivo_f)      ;cerrar archivo
EndFunc   ;==>generar_control_factura
Func Cerrar_factura()
	Sleep(500)
;~ 	_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gestión Integral Versión 11.0.23","AAAAAAAAAABBBBBBBBBB")
	Sleep(500)
;~ 	MouseClick("left", 832, 652, 1)
	MouseClick("left", 979, 395, 1)
	Send("{ESC 2}")
	Sleep(500)
	Send("{ESC 2}")
	Sleep(500)
	Send("{ESC 2}")
	Sleep(2000)
	Send("{ESC 2}")
EndFunc   ;==>Cerrar_factura
Func cerrar_ventanas()
	Sleep(500)
	MouseClick("left", 979, 395, 1)
	Send("{ESC 2}")
	Sleep(500)
	Send("{ESC 2}")
	Sleep(500)
	Send("{ESC 2}")
	Sleep(500)
	Send("{ESC 2}")
	Sleep(500)
	Send("{ESC 2}")
	Send("{ALTDOWN}{F4}{ALTUP}") ;Close waltbot
EndFunc   ;==>cerrar_ventanas

;Configuración Correo
Func Send_Email($RUC, $num_int, $error)
	ShellExecute("C:\Users\rpa\Desktop\Microsoft Office Outlook 2007.lnk") ;Ejecutar Outlook
	$oOL = _OL_Open() ;Abrir Outlook
	Local $dir = $Mail ;Dirección Asociada
	Local $affair = "Error en Clientes Retención" ;Asunto
	Local $body = "<b>Retención RUC :" & $RUC & @CRLF & "numero interno:" & $num_int & @CRLF & "Rechazado por:" & $error
	_OL_Wrapper_SendMail($oOL, $dir, "", "", $affair, $body, "", $olFormatHTML, $olImportanceHigh)
	_OL_Close($oOL) ;Cerrar Outlook
	Sleep(3000)
	Send("{ENTER}") ;Saltar el anuncio de comprobantes sin enviar
	Sleep(3000)

EndFunc   ;==>Send_Email
;~ Funciones Globales
Func _WinWaitActivate($title, $text, $timeout = 0)
	WinWait($title, $text, $timeout)
	If Not WinActive($title, $text) Then WinActivate($title, $text)
	WinWaitActive($title, $text, $timeout)
EndFunc   ;==>_WinWaitActivate
