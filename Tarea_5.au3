
Global $mat
;~ Configuracion del autrecord
Opt('WinWaitDelay', 2000)
Opt('WinDetectHiddenText', 1)
Opt('MouseCoordMode', 0)
;Tarea 5
; com_cobrado    ====================================================================================================================
; Author.........: Juan Pablo Velandia Suarez - jvelandia33@outlook.com
; Name...........: Comprobante cobrado
; Description....: Esta funcion permite obtener el comprobante cobrado en el sistema waltbot
; Syntax.........: com_cobrado($num_interno, $moneda, $value, $tasa_cambio)
; Parameters.....: $num_interno;numero interno de la retenci�n
;~ 				   $moneda;tipo de moneda GUARANI o DOLARES
;~ 				   $value;Valor total de la retenci�n
;~ 				   $tasa_cambio;Valor cambio en este caso utilizado para los dolares
; Return values..: Success  1
;Example.........: com_cobrado($num_interno, $moneda, $value, $tasa_cambio)
;
; ====
Func com_cobrado($num_interno, $moneda, $tasa_cambio, $iva, $renta)
	$iva = StringReplace($iva, ".", "") ;El waltbot no recibe punto
	$renta = StringReplace($renta, ".", "") ;El waltbot no recibe punto
	$tasa_cambio = StringReplace($tasa_cambio, ".", "") ;El waltbot no recibe punto
	Local $value = $iva + $renta
	$num_interno = StringReplace($num_interno, "-", "")
	$num_interno = find_num($num_interno)
	If $moneda = "GUARANI" Then
		$value = $value
	Else
		$value = $value / $tasa_cambio
	EndIf
	Sleep(500)
	_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gesti�n Integral Versi�n 11.0.23", "AAAAAAAAAABBBBBBBBBB")
	MouseClick("left", 555, 351, 1)
	MouseClick("right", 554, 439, 1)
	MouseClick("left", 637, 548, 1)
	Sleep(500)
	_WinWaitActivate("Consulta", "Incluir los Comproba")
	MouseClick("left", 411, 155, 1)
	Sleep(500)
;~ 	Adicion de Funcion si el comprobante no lo encuentra a la primera
	If WinActivate("Filtrar", "") Then ; si no encuentra el recibo
		Sleep(1500)
		_WinWaitActivate("Filtrar", "")
		Send("{ENTER}")
		_WinWaitActivate("Consulta", "")
		Send("{TAB}{TAB}{TAB}{TAB}{TAB}{TAB}{TAB}{ENTER}")
		Sleep(1000)
		new_compro($moneda, $value)
		Return 1
	EndIf
	Sleep(500)
	_WinWaitActivate("Filtrado por N�mero Real", "An&ular y Filtrar")
	Send("{TAB}{TAB}{TAB}{TAB}{TAB}{TAB}{TAB}{ENTER}{DOWN}{HOME}{ENTER}{TAB}")
	Send($num_interno)
	Send("{TAB}")
	Send($num_interno)
	Send("{TAB}{ENTER}")
	Sleep(500)
	_WinWaitActivate("Consulta", "")
	MouseClick("left", 411, 156, 1)
	Sleep(1500)
	If WinActivate("Filtrar", "") Then ; si no encuentra el recibo
		Sleep(500)
		_WinWaitActivate("Filtrar", "")
		Send("{ENTER}")
		_WinWaitActivate("Consulta", "")
		Send("{TAB}{TAB}{TAB}{TAB}{TAB}{TAB}{TAB}{ENTER}")
		Sleep(1000)
		new_compro($moneda, $value)
	Else ;Si lo encuetra seleccionelo
		ConsoleWrite("Comprobante tiene")
		_WinWaitActivate("Filtrado por N�mero Real", "")
		Send("{TAB}{TAB}{TAB}{TAB}{TAB}{ENTER}")
		_WinWaitActivate("Consulta", "")
		Send("{TAB}{TAB}{TAB}{TAB}{TAB}{TAB}{ENTER}")
	EndIf
	Return 1
EndFunc   ;==>com_cobrado
Func new_compro($moneda, $value)
	Local $num_inter_1 = 0
	Local $num_inter_2 = 0
	_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gesti�n Integral Versi�n 11.0.23", "")
	MouseClick("left", 509, 354, 1)
	MouseClick("right", 484, 416, 1)
	MouseClick("left", 508, 425, 1)
	_WinWaitActivate("Comprobante a Cobrar", "")
	Send($num_inter_1)
	Send("{TAB}")
	Send($num_inter_2)
	If $moneda = "Guarani" Then ;GUARANI
		MouseClick("left", 124, 105, 1)
	Else ;DOlares
		MouseClick("left", 129, 123, 1)
	EndIf
	MouseClick("left", 106, 150, 1)
	Sleep(1000)
	Send($value)
	Send("{TAB}")
	MouseClick("left", 577, 54, 1)
EndFunc   ;==>new_compro
Func find_num($num_interno) ;Corrige el numero interno si viene con menos numeros
	Local $var_1 = StringLen($num_interno)
;~ 	ConsoleWrite($var_1 & @CRLF)
	Local $var_2 = StringLeft($num_interno, 6)
;~ 	ConsoleWrite($var_2 & @CRLF)
	Local $var_3 = StringLen($var_2)
;~ 	ConsoleWrite($var_3 & @CRLF)
	Local $var_4 = $var_1 - $var_3
;~ 	ConsoleWrite($var_4 & @CRLF)
	Local $var_5 = StringRight($num_interno, $var_4)
;~ 	ConsoleWrite($var_5 & @CRLF)
	If $var_4 = "8" Then
;~ 		ConsoleWrite("OK numero interno"&@CRLF)
		Return $num_interno
	ElseIf $var_4 <= "8" Then
		Local $var_7 = "0"
		For $i = $var_4 + 1 To 7 Step 1
			$var_7 = $var_7 & "0"
		Next
;~ 		ConsoleWrite($var_7 & @CRLF)
		$num_interno = $var_2 & $var_7 & $var_5
;~ 		ConsoleWrite($num_interno&@CRLF)
		Return $num_interno
	Else
		ConsoleWrite("numero interno" & $num_interno & "mal")
	EndIf
EndFunc   ;==>find_num

