#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Juan Pablo Velandia

 Script Function:
	Tarea Buscar RUC

#ce ----------------------------------------------------------------------------
;~ #include <SendAlert.au3>
;~ Configuracion del autrecord
Opt('WinWaitDelay', 1500)
Opt('WinDetectHiddenText', 1)
Opt('MouseCoordMode', 0)

Func Buscar_RUC($data, $ind)
	_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gestión Integral Versión 11.0.23", "")
	Send("{ALTDOWN}{ALTUP}{RIGHT}{RIGHT}{ENTER}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{DOWN}{ENTER}")
	Sleep(500)
	_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gestión Integral Versión 11.0.23", "")
	Sleep(1000)
	MouseClick("right", 257, 157, 1)
	_WinWaitActivate("Consulta de Clientes", "Mostrar sólo los Cli")
	MouseClick("left", 741, 357, 1)
	MouseClick("left", 741, 357, 1)
	MouseClick("left", 741, 357, 1)
	MouseClick("left", 741, 357, 1)
	MouseClick("left", 618, 167, 1)
	_WinWaitActivate("Filtrado por Número de R.U.C.", "An&ular y Filtrar")
	MouseClick("left", 273, 41, 1)
	Sleep(500)
	Send("{HOME}{ENTER}{TAB}")
	Send($data)
	Sleep(500)
	Send("{TAB}")
	Send($data)
	Sleep(500)
	Send("{TAB}{ENTER}")
	_WinWaitActivate("Consulta de Clientes", "Mostrar sólo los Cli")
	MouseClick("left", 68, 165, 1)
	If WinActivate("Filtrar", "No hay renglónes par") Then ;Si no encuetran el RUC
		_WinWaitActivate("Filtrar", "No hay renglónes par")
		Send("{ENTER}")
		_WinWaitActivate("Consulta de Clientes", "Mostrar sólo los Cli")
		Send("{ESC}")
		_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gestión Integral Versión 11.0.23", "")
		Send("{ESC}")
		_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gestión Integral Versión 11.0.23", "")
		Send("{ESC}")
		Send_Email($data,$ind, "RUC no encontrado")
		ConsoleWrite("Envia correo motivos RUC" & @CRLF)
		Return 0
	Else
		Send("{ESC}")
		_WinWaitActivate("Consulta de Clientes", "")
		Send("{TAB}{TAB}{TAB}{TAB}{TAB}{ENTER}")
		Return 1
	EndIf
EndFunc   ;==>Buscar_RUC
