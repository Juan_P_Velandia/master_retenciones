Opt('WinWaitDelay', 1500)
Opt('WinDetectHiddenText', 1)
Opt('MouseCoordMode', 0)
; Retencion_IVA() ====================================================================================================================
; Author.........: Juan Pablo Velandia Suarez - jvelandia33@outlook.com
; Name...........: Retención IVA tarea 4
; Description....: Esta funcion permite ingresar una retención tipo IVA ,RENTA o Otro al sistema de Waltbot
; Syntax.........: Retencion_IVA($tipo,$F_emision, $orden, $certificado, $imp_retenido, $comprobante_ret)
; Parameters.....: $tipo ; Tipo de la retención IVA , RENTA o otro
;~ 				   $F_emision ;Fecha de emisión de la retención
;~ 				   $$Orden ; numero interno
;~ 				   $certificado ; numero interno
;~ 				   $imp_retenido;Valor de la retención
;~ 				   $comprobante_ret;Campo comprobante retenido que no se utiliza
; Return values..: Success - 1
;Example.........: Retencion_IVA($tipo,$F_emision, $orden, $certificado, $imp_retenido, $comprobante_ret)
;
; ====

Func Retencion_IVA($tipo,$F_emision, $orden, $certificado, $imp_retenido, $comprobante_ret)
	$F_emision = StringReplace($F_emision, "/", "")
	Local $orden_1 =  StringLeft($orden, 4) ; Retrieve 5 characters from the left of the string.
	Local $orden_2 =  StringRight($orden,6) ; Retrieve 5 characters from the right of the string.
	Local $cert_1 = $orden_1
	Local $cert_2 = $orden_2
	Local $comprobante_ret_1 = StringLeft($comprobante_ret,5)
	Local $comprobante_ret_2 = StringRight($comprobante_ret,5)
	Sleep(500)
	_WinWaitActivate(" WALDBOTT SOFTWARE - Sistema de Gestión Integral Versión 11.0.23", "")
	MouseClick("left", 739, 345, 1)
	MouseClick("left", 377, 374, 1)
	Send("{HOME}")
	Sleep(1000)
	Send("{SHIFTDOWN}{END}{SHIFTUP}{BACKSPACE}")
	Send($orden_1) ;ORden Primera Parte
	Send("{TAB}")
	Send("{SHIFTDOWN}{END}{SHIFTUP}{BACKSPACE}")
	Send($orden_2)
	Send("{TAB}") ;Orden  Segunda Parte
	Send($F_emision) ;Fecha de emisión
	MouseMove(698, 435)
	MouseDown("right")
	MouseMove(698, 436)
	MouseUp("right")
	MouseClick("left", 732, 450, 1)
	_WinWaitActivate("Retenciones", "")
	If $tipo = "IVA" Then
	Send("{DOWN}{HOME}{DOWN}{ENTER}")
	ElseIf $tipo = "RENTA" Then
	Send("{DOWN}{HOME}{ENTER}")
	ElseIf $tipo = "OTRO" Then
	Send("{DOWN}{HOME}{DOWN 4}{ENTER}")
	EndIf
	Send($cert_1) ;Certificado 1
	Send("{TAB}")
	Send($cert_2) ;Certificado 2
	Send("{TAB}")
	Send($F_emision) ;Fecha de la emisión 2 do formato
	Send("{TAB}")
	Send($imp_retenido) ;Importe Retenido ML
	Send("{TAB}{TAB}")
	Send($comprobante_ret_1) ;Comprobante Retenido parte 1
	Send("{TAB}")
	Send($comprobante_ret_2) ;Comprobante Retenido parte 2
	Send("{TAB}{ENTER}")
	Return 1
EndFunc   ;==>Retencion_IVA
